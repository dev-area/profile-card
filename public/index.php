<?php

// Common settings
ini_set('display_errors',1);
error_reporting(E_ALL);

session_start();

define('ROOT', dirname(__FILE__, 2));

// vendor
require_once(ROOT .'/vendor/autoload.php');

// load .env variables
$dotenv = Dotenv\Dotenv::createImmutable(ROOT);
$dotenv->load();

//set language
if(isset($_POST['lang'])){
    $localization = App\Components\Localization::getInstance();
    $localization->setLang($_POST['lang']);
}

// Router
$router = new App\Components\Router();
$router->run();