ARG PHP_VERSION=7.4
FROM php:${PHP_VERSION}-fpm

RUN apt-get -y update \
&& apt-get install -y libicu-dev wget git unzip \
&& pecl install xdebug-2.9.4 \
&& docker-php-ext-enable xdebug \
&& docker-php-ext-configure intl \
&& docker-php-ext-install intl pdo pdo_mysql

ADD docker/php/php.ini /usr/local/etc/php/php.ini

RUN wget https://getcomposer.org/installer -O - -q \
    | php -- --install-dir=/bin --filename=composer --quiet

WORKDIR /var/www/profile-card