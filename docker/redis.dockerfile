FROM redis:6.0.3-alpine

COPY docker/redis/etc/redis.conf /usr/local/etc/redis.conf

CMD [ "redis-server", "/usr/local/etc/redis.conf" ]