<?php

namespace App\Components;

use App\Components\Base\Registry;

/**
 * Class Localization
 *
 */
class Localization extends Registry
{

    private string $lang = 'en';

    public function translate($phrase, $num){
        $db = Db::getConnection();
        $lang = 'en';

        //$phrase = 'There %s are %d phrases';

        // sql
        $sql = 'SELECT phrase FROM translation WHERE id = :id AND lang = :lang';

        // Gets result using prepared query
        $result = $db->prepare($sql);

        $result->bindParam(':id', $phrase, \PDO::PARAM_STR);
        $result->bindParam(':lang', $lang, \PDO::PARAM_STR);
        $result->execute();
        $phrase = $result->fetchColumn();

        if($phrase){
            return is_null($num) ? $phrase : sprintf($phrase, $num);
        }

        return $phrase;
    }

    /**
     * @param string $lang
     * @return void
     */
    public function setLang(string $lang): void
    {
        $this->lang = $lang;
    }

    /**
     * @return string
     */
    public function getLang(): string
    {
        return $this->lang;
    }

}