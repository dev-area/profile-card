<?php
namespace App\Components;

use App\Components\Base\Registry;
use Predis;
use Predis\Connection\ConnectionException;

class Redis extends Registry
{
    /**
     * @var array
     */
    private array $host = ['host' => 'redis'];

    /**
     * @var Predis\Client
     */
    private ?Predis\Client $client = null;

    public function __construct(){
        $this->loadPredis();
    }

    private function loadPredis(){
        try {
            $this->client = new Predis\Client($this->host);
            $this->client->connect();
        } catch (ConnectionException $e) {
            // log
        }
    }
    public function setCache($key, $value, $exp = 0){
        $this->client->set($key, $value);
        if (!empty($exp)) {
            $this->client->expire($key, $exp);
        }
    }

    public function getCache($key){
        return $this->client->get($key);
    }

    public function cacheExists($key)
    {
        return $this->client->exists($key);
    }

}