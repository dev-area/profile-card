<?php

if (! function_exists('view')) {
    function view($path, $data, $exp = 0)
    {
        $client = App\Components\Redis::getInstance();
        $lang = App\Components\Localization::getInstance()->getLang();
        $key = $path . '-' . $lang;
        if($client->cacheExists($key)){
            $page = $client->getCache($key);
        }else{
            ob_start();
            require $path;
            $content = trim(ob_get_contents());
            ob_clean();
            require ROOT . '/views/layouts/base.php';
            $page = trim(ob_get_clean());
            $client->setCache($key, $page, $exp);
        }
        $page = rpl($page, $data);
        echo $page;
    }
}

if (! function_exists('tr')) {
    function tr($phrase, $num = null)
    {
        $local = App\Components\Localization::getInstance();
        $phrase = $local->translate($phrase, $num);
        return $phrase;
    }
}

if (! function_exists('rpl')) {
    function rpl($view, array $data)
    {
        foreach ($data as $key => $value) {
            $value = $value ?? 'Unknown';
            $view = str_replace('%' . $key . '%', $value, $view);
        }
        return $view;
    }
}

if (! function_exists('getCountries')) {
    function getCountries()
    {
        return App\Components\Services\CountryService::getCountryList();
    }
}

if (! function_exists('getGenders')) {
    function getGenders()
    {
        return App\Components\Services\GenderService::getGenderList();
    }
}