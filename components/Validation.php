<?php
namespace App\Components;

use App\Components\Base\Registry;
use JsonSchema\Constraints\Constraint;
use JsonSchema\Validator as JsonValidator;

class Validation extends Registry
{
    /**
     * @var Validator
     */
    private ?JsonValidator $validator = null;

    /**
     * @var string
     */
    private string $path = '';

    /**
     * @var array
     */
    private array $invalidFields = [];

    /**
     * Validation constructor.
     */
    public function __construct(){
        $this->path = realpath('../resources/registration.json');
        $this->loadValidator();
    }

    private function loadValidator()
    {
        $this->validator = new JsonValidator;
    }

    public function validate($data){
        $this->validator->validate($data
            , (object)['$ref' => 'file://' . $this->path]
            , Constraint::CHECK_MODE_COERCE_TYPES);

        if (!$this->validator->isValid()) {
            foreach ($this->validator->getErrors() as $error) {
                $this->invalidFields[$error['property']][] = $error['constraint'];
            }
        }

        if (strcmp($data->password, $data->password_confirm) !== 0) {
            $this->invalidFields['password_confirm'][] = 'const';
        }

        $properties = json_decode(file_get_contents($this->path), true)['properties'];
        foreach ($data as $filed => $value)
        {
            if(array_key_exists($filed, $properties)
                && array_key_exists('isNotEmpty', $properties[$filed])
                && empty($value)){
                $this->invalidFields[$filed][] = 'isNotEmpty';
            }
        }

        return $this->invalidFields;
    }

}