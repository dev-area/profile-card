<?php

namespace App\Components;

use PDO;

/**
 * Class Db
 *
 */
class Db
{

    /**
     * @var PDO|null $instance
     */
    private static ?PDO $instance = null;

    /**
     * DB connection
     * @return PDO
     */
    public static function getConnection()
    {
        if (is_null(self::$instance)) {
            // get db params
            $paramsPath = ROOT . '/config/db_params.php';
            /** @noinspection PhpIncludeInspection */
            $params = include($paramsPath);

            // connection
            $dsn = "mysql:host={$params['host']};dbname={$params['dbname']}";
            self::$instance = new PDO($dsn, $params['user'], $params['password']);
            self::$instance->exec("set names utf8");
        }

        return self::$instance;
    }

    private function __construct(){}
    private function __clone(){}
    private function __sleep(){}
    private function __wakeup(){}
}
