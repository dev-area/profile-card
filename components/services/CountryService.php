<?php
namespace App\Components\Services;

use App\Components\Db;

class CountryService
{
    public static function getCountryList(){
        // DB connection
        $db = Db::getConnection();

        // sql
        $sql = 'SELECT * FROM country';

        $result = $db->prepare($sql);
        $result->execute();

        return $result->fetchAll(\PDO::FETCH_KEY_PAIR);
    }
}