<?php


namespace App\Components\Services;


class AvatarService
{

    public static function isFileUploaded($photo){
        return is_uploaded_file($photo['tmp_name']);
    }

    public static function getFileNameWithUpload($photo){
        $fileName = uniqid() . '_' . $photo['name'];
        $location = ROOT . "/public/upload/images/photos/" . $fileName;
        move_uploaded_file($photo['tmp_name'], $location);

        return $fileName;
    }


}