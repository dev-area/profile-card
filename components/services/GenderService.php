<?php
namespace App\Components\Services;

use App\Components\Db;

class GenderService
{

    public static function getGenderList(){
        // DB connection
        $db = Db::getConnection();

        // sql
        $sql = 'SELECT * FROM gender';

        $result = $db->prepare($sql);
        $result->execute();

        return $result->fetchAll(\PDO::FETCH_KEY_PAIR);
    }
}