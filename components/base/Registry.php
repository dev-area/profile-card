<?php
namespace App\Components\Base;

abstract class Registry extends RegistryAbstract
{

    /**
     * {@inheritdoc}
     */
    final public static function getInstance()
    {
        return parent::getInstance();
    }

}