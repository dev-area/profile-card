<?php
namespace App\Components\Base;

abstract class RegistryAbstract
{

    /**
     * @var array
     */
    protected static array $instances = [];

    /**
     * Returns object
     *
     * @return static
     */
    protected static function getInstance()
    {
        $className = static::getClassName();
        if (isset(self::$instances[$className])) {
            if (!(self::$instances[$className] instanceof $className)) {
                self::$instances[$className] = new $className();
            }
        } else {
            self::$instances[$className] = new $className();
        }

        return self::$instances[$className];
    }

    /**
     * Returns class name
     *
     * @return string
     */
    final protected static function getClassName()
    {
        return get_called_class();
    }

    protected function __construct(){}
    final protected function __clone(){}
    final protected function __sleep(){}
    final protected function __wakeup(){}

}