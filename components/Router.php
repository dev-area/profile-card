<?php

namespace App\Components;

/**
 * Class Router
 */
class Router
{

    /**
     * routes
     * @var array 
     */
    private $routes;

    /**
     * construct
     */
    public function __construct()
    {
        // path to routes list
        $routesPath = ROOT . '/config/routes.php';

        /**
         * get routes
         * @noinspection PhpIncludeInspection
         */
        $this->routes = include($routesPath);
    }

    /**
     * Returns URI
     */
    private function getURI()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            return trim($_SERVER['REQUEST_URI'], '/');
        }
    }

    /**
     * Processes any request
     */
    public function run()
    {
        // URI string
        $uri = $this->getURI();

        // Checks the router for existence
        $isUriFound = false;
        foreach ($this->routes as $uriPattern => $path) {

            // Identifies path
            if (preg_match("~^$uriPattern$~", $uri)) {
                $isUriFound = true;

                // Internal route
                $internalRoute = preg_replace("~$uriPattern~", $path, $uri);

                // Defines controller, action, params

                $segments = explode('/', $internalRoute);

                $controllerName = array_shift($segments) . 'Controller';
                $controllerName = ucfirst($controllerName);

                $actionName = 'action' . ucfirst(array_shift($segments));

                $parameters = $segments;

                // Gets and Includes controller file
                $controllerFile = ROOT . '/src/controllers/' . $controllerName . '.php';

                if (file_exists($controllerFile)) {
                    /** @noinspection PhpIncludeInspection */
                    include_once($controllerFile);
                }

                // Creates controller object
                $fullControllerName = "App\\Controllers\\" . $controllerName;
                $controllerObject = new $fullControllerName;

                // Calls action
                $result = call_user_func_array(array($controllerObject, $actionName), $parameters);

                //  In case of success - break
                if ($result != null) {
                    break;
                }
            }
        }

        if(!$isUriFound){
            view(ROOT . '/views/pages/404.php', ["title" => "404"], 30);
            return;
        }
    }

}
