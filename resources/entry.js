import $ from "jquery";
window.$ = $;

import 'bootstrap';

import Popper from 'popper.js/dist/umd/popper.js';
window.Popper = Popper;

import 'bootstrap/dist/css/bootstrap.min.css';
import './main.css';

require('./base.js');