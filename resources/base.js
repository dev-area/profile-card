$(function() {

    const Ajv = require('ajv');
    const ajv = new Ajv({allErrors: true, $data: true, coerceTypes: true});
    ajv.addKeyword('isNotEmpty', {
        type: 'string',
        validate: (schema, data) => {
            return typeof data === 'string' && data.trim() !== ''
        },
        errors: false
    });
    const schema = require('./registration.json');
    const validate = ajv.compile(schema);

    let readURL = function(input) {
        if (input.files && input.files[0]) {

            let reader = new FileReader();
            reader.onload = function (e) {
                $('.avatar').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".file-upload").on('change', function(){
        readURL(this);
    });

    let form = $('[name^=formData]');
    $(form).on('submit', function(e){
        e.preventDefault();
        let action = $(this).attr('action')
        let method = $(this).attr('method')
        let formData = new FormData(this);
        formData.append('submit', '1');

        let values = {};
        for(let pair of formData.entries()) {
            if($.inArray(pair[0], ['photo', 'submit']) !== -1) continue;
            let input = $(this).find(`input[name='${pair[0]}']`);
            $(input).on("click", () => {
                $(input).removeClass('is-invalid');
                $(input).siblings('.invalid').removeClass('invalid-show');
            });
            values[pair[0]] = pair[1];
        }

        let valid = validate(values);
        if (!valid) {
            $.each(validate.errors, ( index, obj ) => {
                let field = obj.dataPath.substring(1);
                let input = `:input[name="${field}"]`;
                $(input).addClass('is-invalid');
                $(input).siblings(`.invalid.${obj.keyword}`).addClass('invalid-show');
            });
        }else{
            $.ajax({
                url: action,
                type: method,
                data: formData,
                contentType: false,
                processData: false,
                success: function(response){
                    let data = JSON.parse(response);
                    //console.log(data);
                    if(!data.success){
                        $.each(data.fields, function( index, values  ) {
                            let field = $.trim(index);
                            let input = `:input[name="${field}"]`;
                            $(input).addClass('is-invalid');
                            $.each(values, ( index, value ) => {
                                let field = $.trim(value);
                                $(input).siblings(`.invalid.${field}`).addClass('invalid-show');
                            });
                        });
                    }else{
                        let URL = '/';
                        if(typeof(data.redirect) !== "undefined"){
                            URL = data.redirect;
                        }
                        $(location).attr('href', URL);
                    }
                },
            });
        }
    });

    let lang = sessionStorage.getItem('lang');
    if(lang !== null){
        $('#lang').val(lang);
    }

    $('#lang').on('change', function() {
        sessionStorage.setItem('lang', $(this).val());
        this.form.submit();
    });

    if($('#country_id').length && $('#country_id').data( "country-id" )){
        let countryId = $('#country_id').data( "country-id" );
        $(`#country_id option[value=${countryId}]`).attr('selected','selected');
    }

    if($('#gender_id').length && $('#gender_id').data( "gender-id" )){
        let genderId = $('#gender_id').data( "gender-id" );
        $(`#gender_id option[value=${genderId}]`).attr('selected','selected');
    }

});


