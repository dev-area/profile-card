<?php

namespace App\Models;


use App\Components\Db;

/**
 * Class User - model
 */
class User
{

    /**
     * User registration
     * @param array $data
     * @return boolean result
     */
    public static function register($data)
    {
        // BD connection
        $db = Db::getConnection();

        // query
        $sql = "INSERT INTO user (first_name, last_name, email, phone, country_id, gender_id, password, photo) VALUES (:first_name, :last_name, :email, :phone, :country_id, :gender_id, :password, :photo)";

        // Gets result using prepared query
        $result = $db->prepare($sql);

        $result->bindValue(':first_name', $data->first_name, \PDO::PARAM_STR);
        $result->bindValue(':last_name', $data->last_name, \PDO::PARAM_STR);
        $result->bindValue(':email', $data->email, \PDO::PARAM_STR);
        $result->bindValue(':phone', $data->phone, \PDO::PARAM_STR);
        $result->bindValue(':country_id', $data->country_id, \PDO::PARAM_INT);
        $result->bindValue(':gender_id', $data->gender_id, \PDO::PARAM_INT);
        $result->bindValue(':password', password_hash($data->password, PASSWORD_DEFAULT), \PDO::PARAM_STR);
        $result->bindValue(':photo', $data->photo, \PDO::PARAM_STR);

        return $result->execute();
    }

    /**
     * Update user data
     * @param array $data
     * @return boolean result
     */
    public static function edit($data)
    {
        // DB connection
        $db = Db::getConnection();

        // SET string
        $setStr = "first_name = :first_name, last_name = :last_name, email = :email,  phone = :phone, country_id = :country_id, gender_id = :gender_id";

        // params
        $params = [':id' => $data->id
                  , ':first_name' => $data->first_name
                  , ':last_name' => $data->last_name
                  , ':email' => $data->email
                  , ':phone' => $data->phone
                  , ':country_id' => $data->country_id
                  , ':gender_id' => $data->gender_id];

        if (isset($data->photo)){
            $params[':photo'] = $data->photo;
            $setStr .= ", photo = :photo";
        }

        // query
        $sql = "UPDATE user SET " . $setStr ." WHERE id = :id";

        // Gets result using prepared query
        $result = $db->prepare($sql);
        return $result->execute($params);
    }

    /**
     * Check for existing with current $email and $password
     * @param string $email e-mail
     * @param string $password password
     * @return mixed : integer user id or false
     */
    public static function checkUserData($email, $password)
    {
        // DB connection
        $db = Db::getConnection();

        // sql
        $sql = 'SELECT id, password FROM user WHERE email = :email';

        // Gets result using prepared query
        $result = $db->prepare($sql);
        $result->bindParam(':email', $email, \PDO::PARAM_STR);
        $result->execute();

        // Gets row
        $user = $result->fetch(\PDO::FETCH_ASSOC);

        if ($user) {
            if(password_verify($password, $user['password'])){
                // Returns user id
                return $user['id'];
            }
        }
        return false;
    }

    /**
     * Stores an user
     * @param integer $userId user id
     */
    public static function auth($userId)
    {
        // Stores user id to session
        $_SESSION['user'] = $userId;
    }

    /**
     * If authorized - returns user id or redirect to login page
     * @return string user id
     */
    public static function checkLogged()
    {
        // If session exists - return user id
        if (isset($_SESSION['user'])) {
            return $_SESSION['user'];
        }

        header("Location: /user/login");
    }

    /**
     * Checks user for guest
     * @return boolean
     */
    public static function isGuest()
    {
        if (isset($_SESSION['user'])) {
            return false;
        }
        return true;
    }

    /**
     * Checks e-mail for existence
     * @param type $email e-mail
     * @return boolean
     */
    public static function checkEmailExists($email)
    {
        // DB connection
        $db = Db::getConnection();

        $userId = 0;
        if(!self::isGuest()){
            $userId =  $_SESSION['user'];
        }

        // SQL query
        $sql = 'SELECT 1 FROM user WHERE email = :email AND id != :userId';

        // Gets result using prepared query
        $result = $db->prepare($sql);
        $result->bindParam(':email', $email, \PDO::PARAM_STR);
        $result->bindParam(':userId', $userId, \PDO::PARAM_INT);
        $result->execute();

        if ($result->fetchColumn())
            return true;
        return false;
    }

    /**
     * Returns user by id
     * @param integer $id user id
     * @param boolean $full sql type
     * @return array user data
     */
    public static function getUserById($id, $full = true)
    {
        // DB connection
        $db = Db::getConnection();

        // SQL query
        if($full){
            $sql = <<<SQL
SELECT u.first_name, u.last_name, u.email, u.phone, u.country_id, u.gender_id, u.photo
FROM user u
WHERE u.id = :id
SQL;
        }else{
            $sql = <<<SQL
SELECT u.first_name, u.last_name, u.email, u.phone, u.photo
, c.name AS country
, g.name AS gender
FROM user u
LEFT JOIN country c ON c.id = u.country_id
LEFT JOIN gender g ON g.id = u.gender_id
WHERE u.id = :id
SQL;
        }

        // Gets result using prepared query
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, \PDO::PARAM_INT);

        // Result as array
        $result->setFetchMode(\PDO::FETCH_ASSOC);
        $result->execute();

        return $result->fetch();
    }

}
