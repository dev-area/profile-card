<?php

namespace App\Controllers;

use App\Components\Services\AvatarService;
use App\Components\Validation;
use App\Models\User;

/**
 * Controller ProfileController
 */
class ProfileController
{
    /**
     * Index action
     */
    public function actionIndex()
    {
        $userId = User::checkLogged();
        $user = User::getUserById($userId, false);

        // Shows view
        view(ROOT . '/views/user/profile/index.php', array_merge($user, ["title" => "Profile"]), 30);
        return true;
    }
    
    /**
     * Edit action
     */
    public function actionEdit()
    {
        if (isset($_POST['submit'])) {

            if(User::isGuest()){
                $response = ['success' => true, 'redirect' => '/user/login'];
                echo json_encode($response);
                return;
            }

            $_POST['password'] = $_POST['password_confirm'] = '123dsdasdas';
            $data = json_decode(json_encode($_POST));

            $invalidFields = Validation::getInstance()->validate($data);

            if (User::checkEmailExists($data->email)) {
                $invalidFields['email'][] = 'isTaken';
            }

            if (!empty($invalidFields)) {
                $response = ['success' => false, 'fields' => $invalidFields];
            } else {
                $data->id = $_SESSION['user'];
                if(AvatarService::isFileUploaded($_FILES['photo'])){
                    $data->photo = AvatarService::getFileNameWithUpload($_FILES['photo']);
                }

                // edit
                $result = User::edit($data);

                $page = $result ? '/profile' : '#';
                $response = ['success' => true, 'redirect' => $page];
            }

            echo json_encode($response);
            return;
        }

        $userId = User::checkLogged();
        $user = User::getUserById($userId);

        // Shows view
        view(ROOT . '/views/user/profile/edit.php', array_merge($user, ["title" => "Edit"]), 30);
    }

}
