<?php

namespace App\Controllers;

use App\Components\Services\AvatarService;
use App\Models\User;
use App\Components\Validation;

/**
 * Controller UserController
 */
class UserController
{
    /**
     * Register action
     */
    public function actionRegister()
    {

        if (isset($_POST['submit'])) {

            $data = json_decode(json_encode($_POST));

            $invalidFields = Validation::getInstance()->validate($data);

            if (User::checkEmailExists($data->email)) {
                $invalidFields['email'][] = 'isTaken';
            }

            if (!empty($invalidFields)) {
                $response = ['success' => false, 'fields' => $invalidFields];
            } else {
                $data->photo = 'avatar_default.png';
                if(AvatarService::isFileUploaded($_FILES['photo'])){
                    $data->photo = AvatarService::getFileNameWithUpload($_FILES['photo']);
                }

                // register user
                $result = User::register($data);
                $page = $result ? '/user/login' : '#';

                $response = ['success' => true, 'redirect' => $page];
            }

            echo json_encode($response);
            return;
        }

        // Shows view
        view(ROOT . '/views/user/auth/register.php', ["title" => "Sign Up"], 30);
    }

    /**
     * Login action
     */
    public function actionLogin()
    {

        // Check form
        if (isset($_POST['submit'])) {

            $_POST['password_confirm'] = $_POST['password'];

            $data = json_decode(json_encode($_POST));

            $invalidFields = Validation::getInstance()->validate($data);

            if (!array_key_exists('email', $invalidFields) && !User::checkEmailExists($data->email)) {
                $response = ['success' => false, 'fields' => ['email' => ['notExist']]];

                echo json_encode($response);
                return;
            }

            if (!array_key_exists('password', $invalidFields) && !array_key_exists('email', $invalidFields)) {
                $userId = User::checkUserData($data->email, $data->password);

                if($userId == false){
                    $response = ['success' => false, 'fields' => ['password' => ['wrongPassword']]];
                }else{
                    User::auth($userId);
                    $response = ['success' => true, 'redirect' => '/profile'];
                }

                echo json_encode($response);
                return;
            }


            $response = ['success' => false, 'fields' => $invalidFields];
            echo json_encode($response);
            return;
        }

        // Shows view
        view(ROOT . '/views/user/auth/login.php', ["title" => "Sing In"], 30);
    }

    /**
     * Logout action
     */
    public function actionLogout()
    {
        // Removes user id from session
        unset($_SESSION["user"]);

        // Redirect to home/login page
        header("Location: /");
    }

}
