<!DOCTYPE html>
<html lang="en">
<head>
    <title>%title%</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link id="favicon" rel="icon" type="image/x-icon" href="/assets/images/favicon.ico" />
    <link href="/assets/main.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <form id="language" action="#" method="post">
    <div class="row justify-content-end">
        <div class="col-auto pt-2">
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="inputGroupSelectLang">Language:</label>
                </div>
                <select name="lang" class="custom-select" id="lang">
                    <option value="en">EN</option>
                    <option value="ru">RU</option>
                </select>
            </div>
         </div>
    </div>
</form>
<?php if (isset($content)): ?>
<?php echo $content; ?>
<?php endif; ?>
</div>
<script src="/assets/main.js"></script>
</body>
</html>