<hr>
<div class="row">
    <div class="col-lg text-right"><a href="/user/logout">Logout</a></div>
</div>
<div class="row">
    <div class="form-group col-lg-3 text-center">
        <img src="/upload//images/photos/%photo%" class="avatar img-circle img-thumbnail">
    </div>
    <div class="form-group col-lg-9">
        <div class="form-row">
            <div class="form-group col-lg-6">
                <label for="first_name"><h4>First name</h4></label>
                <input type="text" class="form-control" name="first_name" id="first_name" value="%first_name%" readonly>
            </div>
            <div class="form-group col-lg-6">
                <label for="last_name"><h4>Last name</h4></label>
                <input type="text" class="form-control" name="last_name" id="last_name" value="%last_name%" readonly>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-lg-6">
                <label for="email"><h4>Email</h4></label>
                <input type="text" class="form-control" name="email" id="email" value="%email%" readonly>
            </div>
            <div class="form-group col-lg-6">
                <label for="phone"><h4>Phone number</h4></label>
                <input type="text" class="form-control" name="phone" id="phone" value="%phone%" readonly>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-lg-6">
                <label for="country"><h4>Country</h4></label>
                <input type="text" class="form-control" name="country" id="country" value="%country%" readonly>
            </div>
            <div class="form-group col-lg-6">
                <label for="gender"><h4>Gender</h4></label>
                <input type="text" class="form-control" name="gender" id="gender" value="%gender%" readonly>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-lg">
                <a href="/profile/edit" class="btn btn-primary btn-lg float-right" role="button" aria-pressed="true">Edit</a>
            </div>
        </div>
    </div>
</div>