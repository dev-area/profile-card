<hr>
<div class="row">
    <div class="col-lg text-right"><a href="/user/logout">Logout</a></div>
</div>
<div>
    <form class="form" id="formDataEdit" name="formDataEdit" action="/profile/edit" method="post" enctype="multipart/form-data">
        <div class="form-row">
            <div class="form-group col-lg-3 text-center">
                <img src="/upload//images/photos/%photo%" class="avatar img-circle img-thumbnail"
                     alt="avatar">
                <h6>Upload a photo...</h6>
                <input type="file" class="file-upload form-control-file text-center center-block" accept="image/png, image/jpeg" name="photo" id="photo">
            </div>
            <div class="form-group col-lg-9">
                <div class="form-row">
                    <div class="form-group col-lg-6">
                        <label for="first_name"><h4>First name*</h4></label>
                        <input type="text" class="form-control" name="first_name" id="first_name"
                               name" title="Enter your first name." value="%first_name%">
                        <div class="invalid isNotEmpty"><?php echo tr('empty_value'); ?></div>
                        <div class="invalid minLength"><?php echo tr('min_length', 2); ?></div>
                        <div class="invalid maxLength"><?php echo tr('max_length', 16); ?></div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="last_name"><h4>Last name*</h4></label>
                        <input type="text" class="form-control" name="last_name" id="last_name"
                               title="Enter your last name." value="%last_name%">
                        <div class="invalid isNotEmpty"><?php echo tr('empty_value'); ?></div>
                        <div class="invalid minLength"><?php echo tr('min_length', 2); ?></div>
                        <div class="invalid maxLength"><?php echo tr('max_length', 26); ?></div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-lg-6">
                        <label for="email"><h4>Email*</h4></label>
                        <input type="text" class="form-control" name="email" id="email"
                               title="Enter your email." value="%email%">
                        <div class="invalid isNotEmpty"><?php echo tr('empty_value'); ?></div>
                        <div class="invalid format"><?php echo tr('email_format'); ?></div>
                        <div class="invalid isTaken"><?php echo tr('email_taken'); ?></div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="phone"><h4>Phone number*</h4></label>
                        <input type="text" class="form-control" name="phone" id="phone"
                               title="Enter your phone." value="%phone%">
                        <div class="invalid isNotEmpty"><?php echo tr('empty_value'); ?></div>
                        <div class="invalid pattern"><?php echo tr('phone_pattern'); ?></div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-lg-6">
                        <label for="country_id"><h4>Country</h4></label>
                        <select id="country_id" name="country_id" class="form-control custom-select" data-country-id="%country_id%">
                            <option value="0">Please select</option>
                            <?php $countries = getCountries(); ?>
                            <?php foreach($countries as $key => $value): ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <div class="invalid country"><?php echo tr('country_list'); ?></div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="gender_id"><h4>Gender</h4></label>
                        <select id="gender_id" name="gender_id" class="form-control custom-select" data-gender-id="%gender_id%">
                            <option value="0">Please select</option>
                            <?php $genders = getGenders(); ?>
                            <?php foreach($genders as $key => $value): ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <div class="invalid gender"><?php echo tr('gender_list'); ?></div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-lg">
                        <input class="btn btn-primary btn-lg float-right" type="submit" name="submit" value="Save">
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>