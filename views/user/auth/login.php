<hr>
<div class="row">
    <div class="col-lg text-right"><a href="/user/register">Register</a></div>
</div>
<div>
    <form class="form" id="formDataLogin" name="formDataLogin" action="/user/login" method="post" id="loginForm">
        <div class="form-row justify-content-center">
            <div class="form-group col-lg-4">
                <label for="email"><h4>Email</h4></label>
                <input type="text" class="form-control" name="email" id="email"
                       placeholder="example@mail.com" title="Enter your email." >
                <div class="valid-feedback">Looks good!</div>
                <div class="invalid isNotEmpty"><?php echo tr('empty_value'); ?></div>
                <div class="invalid format"><?php echo tr('email_format'); ?></div>
                <div class="invalid notExist"><?php echo tr('email_not_exist'); ?></div>
            </div>
        </div>

        <div class="form-row justify-content-center">
            <div class="form-group col-lg-4">
                <label for="password"><h4>Password</h4></label>
                <input type="password" class="form-control" name="password" id="password" placeholder="password"
                       title="Enter your password." >
                <div class="valid-feedback">Looks good!</div>
                <div class="invalid isNotEmpty"><?php echo tr('empty_value'); ?></div>
                <div class="invalid minLength"><?php echo tr('min_length', 6); ?></div>
                <div class="invalid wrongPassword"><?php echo tr('wrong_password', 6); ?></div>
            </div>
        </div>

        <div class="form-row justify-content-center">
            <div class="form-group col-lg-4">
                <input class="btn btn-primary btn-lg float-right" type="submit" name="submit" value="Log In">
            </div>
        </div>
    </form>
</div>