<?php

return [
    'user/register' => 'user/register',
    'user/login' => 'user/login',
    'user/logout' => 'user/logout',
    'profile/edit' => 'profile/edit',
    'profile' => 'profile/index',
    '' => 'user/login',
];