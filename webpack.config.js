const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    entry: {
        'main': './resources/entry.js',
    },
    output: {
        filename: "[name].js",
        path: path.resolve(__dirname, './public/assets')
    },
    resolve: {
        modules: ['node_modules']
    },
    module: {
        rules: [
            {
            test: /\.css$/,
            use: [MiniCssExtractPlugin.loader,
                {loader: 'css-loader'}
                ],
            },
        ],
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "main.css",
            path: path.resolve(__dirname, './public/assets')
        })
    ],
}